var gulp = require('gulp');

var webserver = require('gulp-webserver');

gulp.task('webserver', function () {
    gulp.src(['src', 'bower_components'])
        .pipe(webserver({
            livereload: false,
            directoryListing: false,
            open: true
        }));
});

gulp.task('dev', ['webserver']);
