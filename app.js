'use strict';

angular.module('app', []);

angular.module('app').controller('TestController', function ($scope, FileUploadService) {

    $scope.images = [];
    $scope.quantity = 2;
    $scope.query = "";

    $scope.upload = function (file) {

        var options = {
            url: '/api/ImageUploader',
            fields: {'example': 'value'},
            method: 'POST',
            file: file
        };

        FileUploadService.sendFile(options)
            .then(function success(response) {
                console.log('%c File :' + options.file.name, 'color:white; background:green');
                console.log('%c Response: ' + response.data, 'color:white; background:green');
                $scope.images.push({imageName: options.file.name, status: 'uploaded'})
            }, function error(error) {
                console.log('%c Data : ' + error.data, 'color:white; background:red');
                console.log('%c Status : ' + error.status, 'color:white; background:red');
                console.log('%c Headers : ' + error.headers, 'color:white; background:red');
                console.log('%c FileName : ' + options.file.name, 'color:white; background:red');
                $scope.images.push({imageName: options.file.name, status: 'error'})
            }, function progress(progress) {
                console.log('%c Progress : ' + progress + '%', 'color:white; background:orange');
            });


    }
});

angular.module('app').service('FileUploadService', function ($q) {

    this.sendFile = function (options) {
        var deferred = $q.defer();

        var fd = new FormData();
        fd.append('file', options.file);

        for (var field in options.fields) {
            if (options.fields.hasOwnProperty(field)) {
                fd.append(field, options.fields[field]);
            }
        }

        var xhr = new XMLHttpRequest();
        xhr.open(options.method, options.url, true);

        xhr.upload.onprogress = function (e) {
            if (e.lengthComputable) {
                deferred.notify(e.loaded / e.total * 100);
            }
        };

        xhr.onload = function () {
            if (this.status == 200) {
                deferred.resolve(this);
            } else {
                deferred.reject(this);
            }
        };

        xhr.send(fd);

        return deferred.promise;
    }
});

angular.module('app').directive('fileUploader', function () {
    return {
        restrict: "A",
        scope: {
            onUpload: '&'
        },
        link: function (scope, element) {

            function fileOn(e) {
                e.stopPropagation();
                e.preventDefault();
            }

            function fileDrop(e) {
                fileOn(e);

                var dt = e.dataTransfer || (e.originalEvent && e.originalEvent.dataTransfer);
                var files = e.target.files || (dt && dt.files);

                for (var i = 0, f; f = files[i]; i++) {
                    scope.onUpload({file: f});
                }
            }

            element.on('dragover', fileOn)
                .on('dragleave', fileOn)
                .on('drop', fileDrop);

            element.on('$destroy', function destroy() {
                element.off('$destroy', destroy);
                element.off('dragover', fileOn);
                element.off('dragleave', fileOn);
                element.off('drop', fileDrop);
            });


        }
    }
});