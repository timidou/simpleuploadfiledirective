# Todolist

## Prerequisites

- Git
- NodeJS

## Setup

- `git clone git@bitbucket.org:timidou/simpleuploadfiledirective.git simpleuploadfiledirective`
- `cd simpleuploadfiledirective`
- `npm install`
- `bower install`

## How TO

### Start a development session

- `gulp dev`
- Enjoy : )
